import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { store, persistor } from './src/createStore';

// screens
import UsernameInputScreen from "./src/screens/UsernameInputScreen/UsernameInputScreen";
import PasswordInputScreen from "./src/screens/PasswordInputScreen/PasswordInputScreen";
import RepoInputScreen from "./src/screens/RepoInputScreen/RepoInputScreen";
import CommitsListScreen from "./src/screens/CommitsListScreen/CommitsListScreen";

const Stack = createStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{
                headerStyle: {
                  backgroundColor: '#FFBD20',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                },
              }}
            >
              <Stack.Screen name="UsernameInputScreen" component={UsernameInputScreen} 
                options={() => ({
                  title: "Username"
                })}
              />
              <Stack.Screen name="PasswordInputScreen" component={PasswordInputScreen} 
                options={() => ({
                  title: "Password"
                })}
              />
              <Stack.Screen name="RepoInputScreen" component={RepoInputScreen} 
                options={() => ({
                  title: "Repo",
                  headerLeft: null
                })}
              />
              <Stack.Screen name="CommitsListScreen" component={CommitsListScreen} 
                options={() => ({
                  title: "Commits"
                })}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </PersistGate>
      </Provider>

  );
}
