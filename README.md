![](./assets/demo.gif)

This project is created with [expo](https://expo.io/).
Please [install expo](https://facebook.github.io/react-native/docs/getting-started) if you haven't already.

Usage of yarn is preferred, so please [instal Yarn](https://classic.yarnpkg.com/en/docs/getting-started) if you haven't already.

This instruction will assume you have yarn installed and running.

## Prepare Project

In the project directory, please run:

### `yarn install`

All dependencies should be installed and ready to use.

## Run Project

If you want to test this app on iOS or Android device, please install expo client app on android or ios device.
[ExpoApp iOS](https://apps.apple.com/us/app/expo-client/id982107779)
[ExpoApp Android](https://play.google.com/store/apps/details?id=host.exp.exponent)
Then connects device to the same wireless network as your computer. 

If you want to use iOS Simulator / Android Simulator, please make sure you have on your machine.

In the project directory, please run:

### `expo start`

follow on screen instruction to connect expo client on your device and open app in your device.

Enjoy.