import reducer from './repo';
import * as actions from '../actions/repo';

describe('Repo Reducer', () => {
  it('should have default state', () => {
    const initialState = reducer(undefined, {});
    expect(initialState.repoPath).toEqual('facebook/react-native');
    expect(initialState.commits).toEqual([]);
    expect(initialState.isLoading).toBe(false);
    expect(initialState.repoLoaded).toBe(false);
    expect(initialState.loadRepoFailed).toBe(false);
    expect(initialState.loadRepoError).toBe(null);
  });

  it('should set meta state for repoRequest', () => {
    const initialState = {};
    const action = actions.repoRequest();
    const newState = reducer(initialState, action);
    expect(newState.isLoading).toBe(true);
  });

  it('should set meta state for repoSuccess', () => {
    const initialState = {};
    const data = [{ index: 1 }, { index: 2 }, { index: 3 }, { index: 5 }, { index: 5 }];
    const action = actions.repoSuccess(data);
    const newState = reducer(initialState, action);
    expect(newState.repoLoaded).toBe(true);
    expect(newState.isLoading).toBe(false);
    expect(newState.commits).toEqual(data);
  });

  it('should set meta state for repoFailed', () => {
    const initialState = {};
    const action = actions.repoFailed('Error message');
    const newState = reducer(initialState, action);
    expect(newState.repoLoaded).toBe(false);
    expect(newState.isLoading).toBe(false);
    expect(newState.loadRepoFailed).toBe(true);
    expect(newState.loadRepoError).toEqual('Error message');
  });

  it('should set meta state for repoPath', () => {
    const initialState = {};
    const action = actions.repoPath('newPath/ToRepo');
    const newState = reducer(initialState, action);
    expect(newState.repoPath).toEqual('newPath/ToRepo');
  });

  it('should set meta state for repoOpen', () => {
    const initialState = {};
    const action = actions.repoOpen();
    const newState = reducer(initialState, action);
    expect(newState.repoLoaded).toBe(false);
  });

  it('should set meta state for repoReset', () => {
    const initialState = reducer(undefined, {});

    const data = [{ index: 1 }, { index: 2 }, { index: 3 }, { index: 5 }, { index: 5 }];
    let action = actions.repoSuccess(data);
    let newState = reducer(initialState, action);
    action = actions.repoPath('new/path');
    newState = reducer(newState, action);
    action = actions.repoReset();
    newState = reducer(newState, action);

    expect(newState).toEqual(initialState);
  });
});
