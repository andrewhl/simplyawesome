import types from '../actions/repo';

const initialState = {
  repoPath: 'facebook/react-native',
  commits: [],
  isLoading: false,
  repoLoaded: false,
  loadRepoFailed: false,
  loadRepoError: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.REPO_REQUEST:
      return {
        ...state,
        isLoading: true,
        repoLoaded: false,
        loadRepoFailed: false,
        loadRepoError: null
      };

    case types.REPO_SUCCESS:
      return {
        ...state,
        isLoading: false,
        repoLoaded: true,
        loadRepoFailed: false,
        commits: action.payload,
      };

    case types.REPO_FAILED:
      return {
        ...state,
        isLoading: false,
        repoLoaded: false,
        loadRepoFailed: true,
        loadRepoError: action.payload
      };

    case types.REPO_PATH:
      return {
        ...state,
        repoPath: action.payload
      };

    case types.REPO_OPEN:
      return {
        ...state,
        repoLoaded: false,
        isLoading: false,
      };

    case types.REPO_RESET:
      return {
        ...initialState
      };

    default:
      return state;
  }
};
