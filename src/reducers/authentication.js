import types from '../actions/authentication';

const initialState = {
  user: '',
  password: '',
  authenticated: false,
  isAuthenticating: false,
  isAuthenticationFailed: false,
  authenticationError: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.AUTH_REQUEST:
      return {
        ...state,
        authenticated: false,
        isAuthenticating: true,
        isAuthenticationFailed: false,
        authenticationError: null
      };

    case types.AUTH_SUCCESS:
      return {
        ...state,
        authenticated: true,
        isAuthenticating: false,
        isAuthenticationFailed: false,
      };

    case types.AUTH_FAILED:
      return {
        ...state,
        authenticated: false,
        isAuthenticating: false,
        isAuthenticationFailed: true,
        authenticationError: action.payload
      };

    case types.AUTH_USER:
      return {
        ...state,
        user: action.payload
      };

    case types.AUTH_PSWD:
      return {
        ...state,
        password: action.payload
      };

    case types.AUTH_RESET:
      return {
        ...initialState,
        user: state.user
      };

    default:
      return state;
  }
};
