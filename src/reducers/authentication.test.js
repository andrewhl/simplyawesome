import reducer from './authentication';
import * as actions from '../actions/authentication';

describe('Authentication Reducer', () => {
  it('should have default state', () => {
    const initialState = reducer(undefined, {});
    expect(initialState.user).toEqual('');
    expect(initialState.password).toEqual('');
    expect(initialState.authenticated).toBe(false);
    expect(initialState.isAuthenticating).toBe(false);
    expect(initialState.isAuthenticationFailed).toBe(false);
    expect(initialState.authenticationError).toBe(null);
  });

  it('should set meta state for authRequest', () => {
    const initialState = {};
    const action = actions.authRequest();
    const newState = reducer(initialState, action);
    expect(newState.isAuthenticating).toBe(true);
  });

  it('should set meta state for authSuccess', () => {
    const initialState = {};
    const action = actions.authSuccess();
    const newState = reducer(initialState, action);
    expect(newState.authenticated).toBe(true);
    expect(newState.isAuthenticating).toBe(false);
  });

  it('should set meta state for authFailed', () => {
    const initialState = {};
    const action = actions.authFailed('Error message');
    const newState = reducer(initialState, action);
    expect(newState.authenticated).toBe(false);
    expect(newState.isAuthenticating).toBe(false);
    expect(newState.isAuthenticationFailed).toBe(true);
    expect(newState.authenticationError).toEqual('Error message');
  });

  it('should set meta state for authUser', () => {
    const initialState = {};
    const action = actions.authUser('Username');
    const newState = reducer(initialState, action);
    expect(newState.user).toEqual('Username');
  });

  it('should set meta state for authPassword', () => {
    const initialState = {};
    const action = actions.authPassword('userPassword');
    const newState = reducer(initialState, action);
    expect(newState.password).toEqual('userPassword');
  });

  it('should set meta state for authReset', () => {
    const initialState = reducer(undefined, {});

    let action = actions.authUser('Username');
    let newState = reducer(initialState, action);
    action = actions.authPassword('userPassword');
    newState = reducer(newState, action);
    action = actions.authReset();
    newState = reducer(newState, action);

    expect(newState).toEqual(initialState);
  });
});
