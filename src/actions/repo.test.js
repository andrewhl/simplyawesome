import * as actions from './repo';

describe('repo action', () => {
  it('should return correct action for repoRequest', () => {
    const action = actions.repoRequest();
    expect(action.type).toBe(actions.default.REPO_REQUEST);
  });

  it('should return correct action for repoSuccess', () => {
    const action = actions.repoSuccess();
    expect(action.type).toBe(actions.default.REPO_SUCCESS);
  });

  it('should return correct action for repoFailed', () => {
    const action = actions.repoFailed();
    expect(action.type).toBe(actions.default.REPO_FAILED);
  });

  it('should return correct action for repoPath', () => {
    const action = actions.repoPath('path/to/repo');
    expect(action.type).toBe(actions.default.REPO_PATH);
  });

  it('should return correct action for repoOpen', () => {
    const action = actions.repoOpen();
    expect(action.type).toBe(actions.default.REPO_OPEN);
  });

  it('should return correct action for repoReset', () => {
    const action = actions.repoReset();
    expect(action.type).toBe(actions.default.REPO_RESET);
  });
});
