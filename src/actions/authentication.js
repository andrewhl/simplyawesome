const AUTH_REQUEST = 'AUTH/REQUEST';
const AUTH_SUCCESS = 'AUTH/SUCCESS';
const AUTH_FAILED = 'AUTH/FAILED';
const AUTH_USER = 'AUTH/SETUSER';
const AUTH_PSWD = 'AUTH/SETPASSWORD';
const AUTH_RESET = 'AUTH/RESET';

export default {
  AUTH_REQUEST,
  AUTH_SUCCESS,
  AUTH_FAILED,
  AUTH_USER,
  AUTH_PSWD,
  AUTH_RESET
};

export const authRequest = () => ({
  type: AUTH_REQUEST
});

export const authSuccess = () => ({
  type: AUTH_SUCCESS
});

export const authFailed = (error) => ({
  type: AUTH_FAILED,
  payload: error
});

export const authUser = (user) => ({
  type: AUTH_USER,
  payload: user
});

export const authPassword = (pass) => ({
  type: AUTH_PSWD,
  payload: pass
});

export const authReset = () => ({
  type: AUTH_RESET
});
