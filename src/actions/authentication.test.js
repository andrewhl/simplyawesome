import * as actions from './authentication';

describe('authentication action', () => {
  it('should return correct action for authRequest', () => {
    const action = actions.authRequest();
    expect(action.type).toBe(actions.default.AUTH_REQUEST);
  });

  it('should return correct action for authSuccess', () => {
    const action = actions.authSuccess();
    expect(action.type).toBe(actions.default.AUTH_SUCCESS);
  });

  it('should return correct action for authFailed', () => {
    const action = actions.authFailed();
    expect(action.type).toBe(actions.default.AUTH_FAILED);
  });

  it('should return correct action for authUser', () => {
    const action = actions.authUser('username');
    expect(action.type).toBe(actions.default.AUTH_USER);
  });

  it('should return correct action for authPassword', () => {
    const action = actions.authPassword('userpassword');
    expect(action.type).toBe(actions.default.AUTH_PSWD);
  });

  it('should return correct action for authReset', () => {
    const action = actions.authReset();
    expect(action.type).toBe(actions.default.AUTH_RESET);
  });
});
