const REPO_REQUEST = 'REPO/REQUEST';
const REPO_SUCCESS = 'REPO/SUCCESS';
const REPO_FAILED = 'REPO/FAILED';
const REPO_RESET = 'REPO/RESET';
const REPO_PATH = 'REPO/SETPATH';
const REPO_OPEN = 'REPO/OPEN';

export default {
  REPO_REQUEST,
  REPO_SUCCESS,
  REPO_FAILED,
  REPO_RESET,
  REPO_PATH,
  REPO_OPEN
};

export const repoRequest = () => ({
  type: REPO_REQUEST
});

export const repoSuccess = (commits) => ({
  type: REPO_SUCCESS,
  payload: commits
});

export const repoFailed = (error) => ({
  type: REPO_FAILED,
  payload: error
});

export const repoReset = () => ({
  type: REPO_RESET
});

export const repoPath = (path) => ({
  type: REPO_PATH,
  payload: path
});

export const repoOpen = () => ({
  type: REPO_OPEN
});
