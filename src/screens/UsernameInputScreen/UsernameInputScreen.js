import React from 'react';
import {
  View, SafeAreaView,
  Text, TextInput,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';

import globalStyles from '../../globalStyle';

// actions
import { authUser } from '../../actions/authentication';
import { repoOpen } from '../../actions/repo';

// selectors
import { selectIsUserAuthenticated, selectUser } from '../../selectors/authentication';

class UsernameInputScreen extends React.Component {
  constructor(props) {
    super(props);
    const { user } = this.props;
    this.state = {
      username: user
    };
  }

  componentDidMount() {
    const { userAuthenticated, navigation, repoOpened } = this.props;

    if (userAuthenticated === true) {
      repoOpened();
      navigation.push('PasswordInputScreen');
    }
  }

  handleUsernameChange(value) {
    this.setState(() => ({
      username: value
    }));
  }

  handleConfirmUsername() {
    const { saveUser, navigation } = this.props;

    const { username } = this.state;
    saveUser(username);

    // navigate to password input
    navigation.push('PasswordInputScreen');
  }

  render() {
    const { username } = this.state;
    return (
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.labelContainer}>
            <Text style={styles.label}>Enter your Github Username</Text>
          </View>
          <TextInput
            style={styles.inputText}
            placeholder="Github Username"
            value={username}
            onChangeText={(value) => this.handleUsernameChange(value)}
            autoCapitalize="none"
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.handleConfirmUsername();
            }}
          >
            <Text style={styles.buttonText}>This is my username</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  ...globalStyles
});

const mapStateToProps = (state) => ({
  userAuthenticated: selectIsUserAuthenticated(state),
  user: selectUser(state)
});

const mapDispatchToProps = (dispatch) => ({
  saveUser: (username) => dispatch(authUser(username)),
  repoOpened: () => dispatch(repoOpen())
});

const ConnectedClass = connect(mapStateToProps, mapDispatchToProps)(UsernameInputScreen);

export default function (props) {
  const navigation = useNavigation();

  return <ConnectedClass {...props} navigation={navigation} />;
}
