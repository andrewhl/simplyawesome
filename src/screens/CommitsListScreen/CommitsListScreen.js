import React from 'react';
import {
  View, SafeAreaView,
  Text,
  Image, Button,
  StyleSheet,
  FlatList,
} from 'react-native';
import { useNavigation, } from '@react-navigation/native';
import { connect } from 'react-redux';
import moment from 'moment';

// actions
import { authReset } from '../../actions/authentication';
import { repoOpen } from '../../actions/repo';

// selectors
import { selectRepoCommits } from '../../selectors/repo';
import globalStyle from '../../globalStyle';

// effects

class CommitsListScreen extends React.Component {
  componentDidMount() {
    const { repoOpened } = this.props;
    repoOpened();
  }

  componentDidUpdate() {
    const { userAuthenticated, navigation } = this.props;

    if (!userAuthenticated) {
      navigation.popToTop();
    }
  }

  CommitItem = (singleCommit) => {
    const avatarUrl = (singleCommit.author && singleCommit.author.avatar_url) || singleCommit.committer.avatar_url;
    const author = (singleCommit.author && singleCommit.author.login) || singleCommit.commit.author.name;
    const commitDate = singleCommit.commit.author.date;
    const commitMoment = moment(commitDate);
    const diffMinutes = moment().diff(commitMoment, 'minutes');
    const diffHours = Math.round(diffMinutes / 60);
    const diffDays = Math.round(diffMinutes / 60 / 24);

    let displayDate = `${diffMinutes} minutes ago`;
    if (diffMinutes === 1) {
      displayDate = `${diffMinutes} minute ago`;
    } else if (diffHours < 24) {
      displayDate = `${diffHours} hours ago`;
      if (diffHours === 1) {
        displayDate = `${diffHours} hour ago`;
      }
    } else if (diffDays < 24) {
      displayDate = `${diffDays} days ago`;
      if (diffDays === 1) {
        displayDate = `${diffDays} day ago`;
      }
    }

    return (
      <View style={styles.item}>
        <View style={styles.itemRow}>
          <Image
            style={styles.authorImage}
            source={{
              uri: avatarUrl,
            }}
          />
          <View>
            <Text style={styles.authorName}>{author}</Text>
            <Text style={styles.commitDate}>{displayDate}</Text>
          </View>
        </View>

        <Text>{singleCommit.commit.message}</Text>
      </View>
    );
  };

  render() {
    const { repoCommits } = this.props;

    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          data={repoCommits}
          renderItem={({ item }) => this.CommitItem(item)}
          keyExtractor={(item) => item.sha}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  ...globalStyle,
  item: {
    backgroundColor: '#f5e6c4',
    padding: 12,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  itemRow: {
    flexDirection: 'row',
    marginBottom: 6,
  },
  authorName: {
    fontSize: 16,
  },
  commitDate: {
    fontSize: 12,
    color: '#555'
  },
  authorImage: {
    width: 40,
    height: 40,
    marginRight: 6,
  }
});


const mapStateToProps = (state) => ({
  repoCommits: selectRepoCommits(state),
});

const mapDispatchToProps = (dispatch) => ({
  doLogout: () => dispatch(authReset()),
  repoOpened: () => dispatch(repoOpen()),
});

function CommitsListScreenNaved(props) {
  const navigation = useNavigation();
  const { doLogout } = props;

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button color="#fff" onPress={() => doLogout()} title="Logout" />
      ),
    });
  }, [navigation, doLogout]);

  return <CommitsListScreen {...props} navigation={navigation} />;
}

export default connect(mapStateToProps, mapDispatchToProps)(CommitsListScreenNaved);
