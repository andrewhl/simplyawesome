import React from 'react';
import {
  View, SafeAreaView,
  Text, TextInput,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import LoadingOverlay from 'react-native-orientation-loading-overlay';

import globalStyles from '../../globalStyle';

// actions
import { authPassword } from '../../actions/authentication';

// selectors
import {
  selectIsAuthenticating, selectIsUserAuthenticated, selectPassword,
  selectUser, selectIsAuthenticationFailed, selectAuthenticationError
} from '../../selectors/authentication';

// effects
import { doGithubAuth } from '../../effects/authentication';

class PasswordInputScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: ''
    };
  }

  componentDidMount() {
    const { userAuthenticated, navigation } = this.props;

    if (userAuthenticated === true) {
      navigation.push('RepoInputScreen');
    }
  }

  componentDidUpdate() {
    const { userAuthenticated, navigation } = this.props;
    if (userAuthenticated === true) {
      navigation.push('RepoInputScreen');
    }
  }

  handlepasswordChange(value) {
    this.setState(() => ({
      password: value
    }));
  }

  handleLogin() {
    const { savePassword, doLogin } = this.props;
    const { password } = this.state;
    savePassword(password);
    doLogin();
  }

  render() {
    const {
      user, isAuthenticating, isAuthenticationFailed, authenticationError
    } = this.props;
    return (
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.labelContainer}>
            <Text style={styles.label}>
              Enter password for
              <Text style={styles.labelUsername}>{user}</Text>
            </Text>
          </View>
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Password"
            onChangeText={(value) => this.handlepasswordChange(value)}
            defaultValue=""
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.handleLogin();
            }}
          >
            <Text style={styles.buttonText}>Login</Text>
          </TouchableOpacity>
          {isAuthenticationFailed
            && (
            <View style={styles.errorContainer}>
              <Text style={styles.errorLabel}>{authenticationError}</Text>
            </View>
            )}
        </View>
        <LoadingOverlay
          visible={isAuthenticating}
          color="white"
          indicatorSize="large"
          messageFontSize={24}
          message="Loading..."
        />
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
  ...globalStyles,
  labelUsername: {
    ...globalStyles.label,
    color: '#000',
  }
});

const mapStateToProps = (state) => ({
  userAuthenticated: selectIsUserAuthenticated(state),
  user: selectUser(state),
  password: selectPassword(state),
  isAuthenticating: selectIsAuthenticating(state),
  isAuthenticationFailed: selectIsAuthenticationFailed(state),
  authenticationError: selectAuthenticationError(state),
});

const mapDispatchToProps = (dispatch) => ({
  doLogin: () => dispatch(doGithubAuth()),
  savePassword: (password) => dispatch(authPassword(password)),
});

const ConnectedClass = connect(mapStateToProps, mapDispatchToProps)(PasswordInputScreen);

export default function (props) {
  const navigation = useNavigation();

  return <ConnectedClass {...props} navigation={navigation} />;
}
