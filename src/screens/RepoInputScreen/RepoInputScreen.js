import React from 'react';
import {
  View, SafeAreaView,
  Text, TextInput,
  Button, TouchableOpacity,
  StyleSheet
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import LoadingOverlay from 'react-native-orientation-loading-overlay';


import globalStyles from '../../globalStyle';

// actions
import { authReset, } from '../../actions/authentication';
import { repoPath, } from '../../actions/repo';

// selectors
import { selectIsUserAuthenticated } from '../../selectors/authentication';
import {
  selectRepoPath, selectRepoLoaded, selectRepoIsLoading, selectLoadRepoFailed, selectLoadRepoError
} from '../../selectors/repo';

// effects
import { getRepoCommits } from '../../effects/repo';

class RepoInputScreen extends React.Component {
  constructor(props) {
    super(props);
    const { repo } = props;
    this.state = {
      repoPath: repo
    };
  }

  componentDidMount() {
    const { userAuthenticated, navigation } = this.props;

    if (!userAuthenticated) {
      navigation.popToTop();
    }
  }

  componentDidUpdate() {
    const { userAuthenticated, newRepoLoaded, navigation } = this.props;

    if (newRepoLoaded) {
      navigation.push('CommitsListScreen');
    }

    if (!userAuthenticated) {
      navigation.popToTop();
    }
  }

  handleRepoChange(value) {
    this.setState(() => ({
      repoPath: value
    }));
  }

  handleGetRepo() {
    const { saveRepoPath, getCommits } = this.props;
    const { repoPath } = this.state;

    saveRepoPath(repoPath);
    getCommits();
  }

  render() {
    const { isLoadingRepo, isLoadRepoFailed, loadRepoError } = this.props;
    const { repoPath } = this.state;

    return (
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.labelContainer}>
            <Text style={styles.label}>Enter repo path</Text>
          </View>
          <TextInput
            style={styles.inputText}
            placeholder="github repo"
            onChangeText={(value) => this.handleRepoChange(value)}
            value={repoPath}
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.handleGetRepo();
            }}
          >
            <Text style={styles.buttonText}>Check commits</Text>
          </TouchableOpacity>
          {isLoadRepoFailed
            && (
            <View style={styles.errorContainer}>
              <Text style={styles.errorLabel}>{loadRepoError}</Text>
            </View>
            )}
        </View>
        <LoadingOverlay
          visible={isLoadingRepo}
          color="white"
          indicatorSize="large"
          messageFontSize={24}
          message="Loading..."
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  ...globalStyles
});

const mapStateToProps = (state) => ({
  userAuthenticated: selectIsUserAuthenticated(state),
  repo: selectRepoPath(state),
  newRepoLoaded: selectRepoLoaded(state),
  isLoadingRepo: selectRepoIsLoading(state),
  isLoadRepoFailed: selectLoadRepoFailed(state),
  loadRepoError: selectLoadRepoError(state)
});

const mapDispatchToProps = (dispatch) => ({
  saveRepoPath: (path) => dispatch(repoPath(path)),
  getCommits: () => dispatch(getRepoCommits()),
  doLogout: () => dispatch(authReset()),
});


// let ConnectedClass = connect(mapStateToProps,mapDispatchToProps)(RepoInputScreen);

function RepoInputScreenNaved(props) {
  const navigation = useNavigation();
  const { doLogout } = props;

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button color="#fff" onPress={() => doLogout()} title="Logout" />
      ),
    });
  }, [navigation, doLogout]);

  return <RepoInputScreen {...props} navigation={navigation} />;
}

export default connect(mapStateToProps, mapDispatchToProps)(RepoInputScreenNaved);
