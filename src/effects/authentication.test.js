import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import * as effects from './authentication';

describe('auth Effects', () => {
  it('should dispatch success action', async () => {
    const mockStore = configureMockStore([thunk]);
    const store = mockStore({
      repo: {},
      auth: {
        user: 'username',
        password: 'userPassword',
        authenticated: false,
        isAuthenticating: false,
        isAuthenticationFailed: false,
        authenticationError: null
      }
    });

    fetchMock.mock('https://api.github.com/user', {
      status: 200,
      body: {
        login: 'userloginname',
        id: 678774,
      }
    });

    const expectedActions = [
      { type: 'AUTH/REQUEST' },
      { type: 'AUTH/SUCCESS' },
    ];

    await store.dispatch(effects.doGithubAuth());
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch failed action', async () => {
    const mockStore = configureMockStore([thunk]);
    const store = mockStore({
      repo: {},
      auth: {
        user: 'username',
        password: 'userPassword',
        authenticated: false,
        isAuthenticating: false,
        isAuthenticationFailed: false,
        authenticationError: null
      }
    });

    fetchMock.mock('https://api.github.com/user', {
      status: 401,
      body: {
        message: 'Bad credentials',
        documentation_url: 'https://developer.github.com/v3'
      }
    },
    { overwriteRoutes: true });

    const expectedActions = [
      { type: 'AUTH/REQUEST' },
      { type: 'AUTH/FAILED', payload: 'Bad credentials' },
    ];

    await store.dispatch(effects.doGithubAuth());
    expect(store.getActions()).toEqual(expectedActions);
  });
});
