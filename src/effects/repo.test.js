import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import * as effects from './repo';

describe('repo Effects', () => {
  it('should dispatch success action', async () => {
    const repoPath = 'facebook/react-native';
    const mockStore = configureMockStore([thunk]);
    const store = mockStore({
      repo: {
        repoPath,
        commits: [],
        isLoading: false,
        repoLoaded: false,
        loadRepoFailed: false,
        loadRepoError: null,
      },
      auth: {
        user: 'username',
        password: 'userPassword',
        authenticated: false,
        isAuthenticating: false,
        isAuthenticationFailed: false,
        authenticationError: null
      }
    });

    const data = [{ index: 1 }, { index: 2 }, { index: 3 }];
    fetchMock.mock('https://api.github.com/repos/facebook/react-native/commits', {
      status: 200,
      body: data
    });

    const expectedActions = [
      { type: 'REPO/REQUEST' },
      { type: 'REPO/SUCCESS', payload: data },
    ];

    await store.dispatch(effects.getRepoCommits());
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch failed action', async () => {
    const repoPath = 'facebook/react-anative';
    const mockStore = configureMockStore([thunk]);
    const store = mockStore({
      repo: {
        repoPath,
        commits: [],
        isLoading: false,
        repoLoaded: false,
        loadRepoFailed: false,
        loadRepoError: null,
      },
      auth: {
        user: 'username',
        password: 'userPassword',
        authenticated: false,
        isAuthenticating: false,
        isAuthenticationFailed: false,
        authenticationError: null
      }
    });

    fetchMock.mock('https://api.github.com/repos/facebook/react-anative/commits', {
      status: 404,
      body: {
        message: 'Not Found',
        documentation_url: 'https://developer.github.com/v3/repos/commits/#list-commits-on-a-repository'
      }
    },
    { overwriteRoutes: true });

    const expectedActions = [
      { type: 'REPO/REQUEST' },
      { type: 'REPO/FAILED', payload: 'Not Found' },
    ];

    await store.dispatch(effects.getRepoCommits());
    expect(store.getActions()).toEqual(expectedActions);
  });
});
