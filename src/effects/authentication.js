import base64 from 'react-native-base64';
import { authRequest, authFailed, authSuccess } from '../actions/authentication';
import { selectUser, selectPassword } from '../selectors/authentication';

export function doGithubAuth() {
  return async (dispatch, getState) => {
    dispatch(authRequest());

    const user = selectUser(getState());
    const password = selectPassword(getState());

    const plain = `${user}:${password}`;
    const encoded = base64.encode(plain);

    const url = 'https://api.github.com/user';

    const requestOptions = {
      method: 'GET',
      headers: {
        Authorization: `Basic ${encoded}`,
      },
    };

    return fetch(url, requestOptions)
      .then((response) => response.json())
      .then((jsonObj) => {
        const login = jsonObj.login || '';
        const id = jsonObj.id || 0;
        const { message } = jsonObj;

        if (login.length > 0 && id > 0) {
          // success
          dispatch(authSuccess());
        } else {
          // fail
          dispatch(authFailed(message));
        }
      })
      .catch(() => {
        dispatch(authFailed('Unable to connect'));
      });
  };
}
