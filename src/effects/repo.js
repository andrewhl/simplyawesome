import base64 from 'react-native-base64';
import { selectUser, selectPassword } from '../selectors/authentication';
import { selectRepoPath } from '../selectors/repo';
import { repoRequest, repoFailed, repoSuccess } from '../actions/repo';


export function getRepoCommits() {
  return async (dispatch, getState) => {
    dispatch(repoRequest());

    const user = selectUser(getState());
    const password = selectPassword(getState());
    const repo = selectRepoPath(getState());

    const plain = `${user}:${password}`;
    const encoded = base64.encode(plain);

    const url = `https://api.github.com/repos/${repo}/commits`;
    const requestOptions = {
      method: 'GET',
      headers: {
        Authorization: `Basic ${encoded}`,
      },
    };

    return fetch(url, requestOptions)
      .then((response) => response.json())
      .then((jsonObj) => {
        const { message } = jsonObj;

        if (Array.isArray(jsonObj) && jsonObj.length > 0) {
          // success
          dispatch(repoSuccess(jsonObj));
        } else {
          // fail
          dispatch(repoFailed(message));
        }
      })
      .catch(() => {
        dispatch(repoFailed('Unable to connect'));
      });
  };
}
