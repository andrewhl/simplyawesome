import * as selectors from './authentication';

describe('Authentication selectors', () => {
  it('Should select value for selectUser', () => {
    const state = { auth: { user: 'myUsername' }, };
    const result = selectors.selectUser(state);
    expect(result).toEqual('myUsername');
  });

  it('Should select value for selectPassword', () => {
    const state = { auth: { password: 'myPassword' }, };
    const result = selectors.selectPassword(state);
    expect(result).toEqual('myPassword');
  });

  it('Should select value for selectIsUserAuthenticated', () => {
    const state = { auth: { authenticated: true }, };
    const result = selectors.selectIsUserAuthenticated(state);
    expect(result).toBe(true);
  });

  it('Should select value for selectIsAuthenticating', () => {
    const state = { auth: { isAuthenticating: true }, };
    const result = selectors.selectIsAuthenticating(state);
    expect(result).toBe(true);
  });

  it('Should select value for selectIsAuthenticationFailed', () => {
    const state = { auth: { isAuthenticationFailed: false }, };
    const result = selectors.selectIsAuthenticationFailed(state);
    expect(result).toBe(false);
  });

  it('Should select value for selectAuthenticationError', () => {
    const state = { auth: { authenticationError: 'Hello world' }, };
    const result = selectors.selectAuthenticationError(state);
    expect(result).toEqual('Hello world');
  });
});
