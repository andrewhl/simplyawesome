import * as selectors from './repo';

describe('Authentication selectors', () => {
  it('Should select value for selectRepoPath', () => {
    const state = { repo: { repoPath: 'path/toRepo' }, };
    const result = selectors.selectRepoPath(state);
    expect(result).toEqual('path/toRepo');
  });

  it('Should select value for selectRepoCommits', () => {
    const data = [{ index: 1 }, { index: 2 }, { index: 3 }, { index: 4 }, { index: 5 }];
    const state = { repo: { commits: data }, };
    const result = selectors.selectRepoCommits(state);
    expect(result).toEqual(data);
  });

  it('Should select value for selectRepoIsLoading', () => {
    const state = { repo: { isLoading: true }, };
    const result = selectors.selectRepoIsLoading(state);
    expect(result).toBe(true);
  });

  it('Should select value for selectRepoLoaded', () => {
    const state = { repo: { repoLoaded: true }, };
    const result = selectors.selectRepoLoaded(state);
    expect(result).toBe(true);
  });

  it('Should select value for selectLoadRepoFailed', () => {
    const state = { repo: { loadRepoFailed: false }, };
    const result = selectors.selectLoadRepoFailed(state);
    expect(result).toBe(false);
  });

  it('Should select value for selectLoadRepoError', () => {
    const state = { repo: { loadRepoError: 'Hello world' }, };
    const result = selectors.selectLoadRepoError(state);
    expect(result).toEqual('Hello world');
  });
});
