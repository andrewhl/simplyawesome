export function selectRepoPath(state) {
  return state.repo.repoPath;
}

export function selectRepoCommits(state) {
  return state.repo.commits;
}

export function selectRepoIsLoading(state) {
  return state.repo.isLoading;
}

export function selectLoadRepoFailed(state) {
  return state.repo.loadRepoFailed;
}

export function selectLoadRepoError(state) {
  return state.repo.loadRepoError;
}

export function selectRepoLoaded(state) {
  return state.repo.repoLoaded;
}
