export function selectUser(state) {
  return state.auth.user;
}

export function selectPassword(state) {
  return state.auth.password;
}

export function selectIsUserAuthenticated(state) {
  return state.auth.authenticated;
}

export function selectIsAuthenticating(state) {
  return state.auth.isAuthenticating;
}

export function selectIsAuthenticationFailed(state) {
  return state.auth.isAuthenticationFailed;
}

export function selectAuthenticationError(state) {
  return state.auth.authenticationError;
}
