import { StyleSheet } from 'react-native';

const globalStyle = StyleSheet.create({
  container: {
    padding: 25,
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  labelContainer: {
    alignItems: 'flex-start',
    width: '100%',
  },
  label: {
    color: '#4E5154',
    fontSize: 22,
    marginBottom: 8
  },
  inputText: {
    color: '#2F2F2F',
    height: 40,
    borderColor: '#000',
    borderWidth: 1,
    borderRadius: 10,
    width: '100%',
    paddingLeft: 6,
    paddingRight: 6,
    fontSize: 18
  },
  button: {
    backgroundColor: '#FFBD20',
    height: 40,
    marginTop: 12,
    width: '100%',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#FFF',
    fontSize: 20,
    fontWeight: '700',
  },
  errorContainer: {
    width: '100%',
    height: 40,
    alignItems: 'center',
    marginTop: 8
  },
  errorLabel: {
    color: '#E00032'
  }
});

export default globalStyle;
