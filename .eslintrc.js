/* eslint-disable quote-props */
module.exports = {
  'extends': 'airbnb',
  'parser': 'babel-eslint',
  'env': {
    'jest': true,
  },
  'rules': {
    'no-use-before-define': 'off',
    'react/jsx-filename-extension': 'off',
    'react/prop-types': 'off',
    'comma-dangle': 'off',
    'import/prefer-default-export': 'off',
    'react/jsx-props-no-spreading': 'off',
    'func-names': 'off',
    'max-len': 'off',
    'no-shadow': 'off'
  },
  'globals': {
    'fetch': false
  }
};
